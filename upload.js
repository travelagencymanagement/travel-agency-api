const path = require('path');
const fs = require('fs');
const { google } = require('googleapis');

//https://drive.google.com/uc?export=view&id=[image_id]

// These id's and secrets should come from .env file.
const CLIENT_ID = '93508021990-9bn8nv80hgslhg9iflikfsb4ejpluqba.apps.googleusercontent.com';
const CLIENT_SECRET = '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';
const REDIRECT_URI = 'https://developers.google.com/oauthplayground';
const REFRESH_TOKEN = '1//04ra7mTTfFn7ICgYIARAAGAQSNwF-L9IraEGCOGuOiyQNcaR7CiGOnKB_Tg-Xjadz8zpWIykkExvWKDgmYg5fpbnDAAkdyylXOvI';

const FOLDER_ID = '1ExzdnIli5ssEH3c0VIpsz8G2NTkwucMB'

const oAuth2Client = new google.auth.OAuth2(
  CLIENT_ID,
  CLIENT_SECRET,
  REDIRECT_URI
);

oAuth2Client.setCredentials({ refresh_token: REFRESH_TOKEN });

const drive = google.drive({
  version: 'v3',
  auth: oAuth2Client,
});

const filePath = path.join(__dirname, 'offer-1.png');

async function uploadFile() {
  try {
    const response = await drive.files.create({
      requestBody: {
        name: 'example.png', // This can be name of your choice
        mimeType: 'image/png',
        parents: [FOLDER_ID],
      },
      media: {
        mimeType: 'image/png',
        body: fs.createReadStream(filePath),
      },
    });

    console.log(response.data);
  } catch (error) {
    console.log(error.message);
  }
}

// uploadFile()

async function deleteFile() {
  try {
    const response = await drive.files.delete({
      fileId: '1bW_ws75Zc_d26s38sn3nJzVagulJZsaU',
    });
    console.log(response.data, response.status);
  } catch (error) {
    console.log(error.message);
  }
}

deleteFile()

async function generatePublicUrl() {
  try {
    const fileId = '12PVMC1znqe6B5bm4Rc1mBaf5emSlD8y2';
    await drive.permissions.create({
      fileId: fileId,
      requestBody: {
        role: 'reader',
        type: 'anyone',
      },
    });

    /* 
    webViewLink: View the file in browser
    webContentLink: Direct download link 
    */
    const result = await drive.files.get({
      fileId: fileId,
      fields: 'webViewLink, webContentLink',
    });
    console.log(result.data);
  } catch (error) {
    console.log(error.message);
  }
}

// generatePublicUrl()
