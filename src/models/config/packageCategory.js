const mongoose = require('mongoose');

const { Schema, model } = mongoose;
const { messages } = require('../../utils');
const { required } = messages
// local, out station, fixed.
const packageCategorySchema = Schema({
  _id: Schema.Types.ObjectId,
  consumerId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required,
  },
  name: {
    type: String,
    required,
    trim: true
  },
  comment: {
    type: String,
    maxlength: 200,
    trim: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
})

module.exports = model('PackageCategory', packageCategorySchema);
