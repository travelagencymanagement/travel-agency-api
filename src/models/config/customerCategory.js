const mongoose = require('mongoose');

const { Schema, model } = mongoose;
const { messages } = require('../../utils');
const { required } = messages
// regular, operators
const customerCategorySchema = Schema({
  _id: Schema.Types.ObjectId,
  consumerId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required,
    unique: false,
  },
  name: {
    type: String,
    required,
    trim: true
  },
  comment: {
    type: String,
    maxlength: 200,
    trim: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
})

customerCategorySchema.index({ name: 1, consumerId: 1 }, { unique: true })

module.exports = model('CustomerCategory', customerCategorySchema);
