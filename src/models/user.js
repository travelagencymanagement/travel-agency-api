const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const { Schema, model } = mongoose;

const { validationPattern, messages } = require('../utils');
const { email } = validationPattern
const { required, invalidEmail } = messages

const userSchema = Schema({
  _id: Schema.Types.ObjectId,
  firstName: {
    type: String,
    required,
    trim: true,
  },
  lastName: {
    type: String,
    required,
    trim: true,
  },
  password: {
    type: String,
    required,
  },
  email: {
    type: String,
    required,
    unique: true,
    uniqueCaseInsensitive: true,
    match: [email, invalidEmail],
    trim: true
  },
  isActive: {
    type: Boolean,
    default: false,
  },
  profile: {
    type: String,
  },
  agencyName: {
    type: String,
  },
  agencyRegistrationNo: {
    type: String,
  },
  address: {
    addressLine1: {
      type: String,
    },
    addressLine2: {
      type: String,
    },
    city: {
      type: String,
    },
    state: {
      type: String,
    },
    pinCode: {
      type: String,
    }
  },
  primaryContact: {
    type: String,
  },
  secondaryContact: {
    type: String,
  },
  websiteLink: {
    type: String,
  },
  linkedInLink: {
    type: String,
  },
  facebookLink: {
    type: String,
  },
  instagramLink: {
    type: String,
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
})

userSchema.plugin(uniqueValidator);

module.exports = model('User', userSchema);
