
const { getYear, getMonth } = require('./date');

const validationPattern = {
  contact: /^([0-9]{10})+$/,
  email: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
  number: /^[0-9]+$/
}

const messages = {
  required: 'Required.',
  invalidEmail: 'Invalid Email Id.',
  invalidContact: 'Invalid Contact.'
}

const amountDifference = (amount = 0, originalAmount = 0) => {
  const amountInNumber = Number(amount)
  const originalAmountInNumber = Number(originalAmount)
  return amountInNumber - originalAmountInNumber
}

const isYearMonthUpdated = (date, originalDate) => getYear(date) !== getYear(originalDate) || getMonth(date) !== getMonth(originalDate)

const monthList = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

const formatAmount = (data) => Number(data).toLocaleString('en-IN', {
  maximumFractionDigits: 2,
  minimumFractionDigits: 2,
})

const numberToTime = (number) => {
  if (number < 0) {
    return '0:0'
  }
  const num = number * 60
  const hours = Math.floor(num / 60)
  const minutes = num % 60
  return `${hours}:${minutes}`
}

module.exports = {
  validationPattern,
  messages,
  amountDifference,
  isYearMonthUpdated,
  monthList,
  formatAmount,
  numberToTime,
}
