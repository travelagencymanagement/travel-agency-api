const express = require('express');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const nodemailer = require('nodemailer');
const { google } = require('googleapis');

const router = express.Router();

const User = require('../models/user');

const CustomerCategory = require('../models/config/customerCategory');
const ExpenseCategory = require('../models/config/expenseCategory');
const ExpenseType = require('../models/config/expenseType');
const PackageCategory = require('../models/config/packageCategory');
const PaymentMethod = require('../models/config/paymentMethod');
// const RequestCategory = require('../models/config/requestCategory');
const RequestType = require('../models/config/requestType');
const StaffCategory = require('../models/config/staffCategory');
const VehicleCategory = require('../models/config/vehicleCategory');
const VehicleType = require('../models/config/vehicleType');


// These id's and secrets should come from .env file.
const CLIENT_ID = '93508021990-9bn8nv80hgslhg9iflikfsb4ejpluqba.apps.googleusercontent.com';
const CLIENT_SECRET = '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';
const REDIRECT_URI = 'https://developers.google.com/oauthplayground';
const REFRESH_TOKEN = '1//04j9hLGtF8k0xCgYIARAAGAQSNwF-L9IrAAY7M9La_xOhZxE23Gg60jVzuzNJCb6A32Ll9y4t58DU2ugCdQUddeaan-pBikcMUDw';

const oAuth2Client = new google.auth.OAuth2(
  CLIENT_ID,
  CLIENT_SECRET,
  REDIRECT_URI
);

oAuth2Client.setCredentials({ refresh_token: REFRESH_TOKEN });

const sendUserEmail = async (mailOptions) => {
  const accessToken = await oAuth2Client.getAccessToken();

  const transport = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      type: 'OAuth2',
      user: 'travelzagency22@gmail.com',
      clientId: CLIENT_ID,
      clientSecret: CLIENT_SECRET,
      refreshToken: REFRESH_TOKEN,
      accessToken: accessToken,
    },
  });
  const result = await transport.sendMail(mailOptions);
  return result;
}

const sendEmail = async (link, email) => {
  try {
    const mailOptions = {
      from: 'TRAVEL AGENCY <travelzagency22@gmail.com>',
      to: email,
      subject: 'RESET password - Travel agency',
      text: `Here is the reset link ${link}`,
      html: `
        <!doctype html>
        <html lang="en-US">
        <head>
            <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
            <title>Travel agency reset password</title>
            <meta name="description" content="Travel agency reset password">
            <style type="text/css">
                a:hover {text-decoration: underline !important;}
            </style>
        </head>

        <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
            <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8">
                <tr>
                    <td>
                        <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                            align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0"
                                        style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                        <tr>
                                            <td style="height:40px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="padding:0 35px;">
                                                <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:'Rubik',sans-serif;">
                                                You have requested to reset your password
                                                </h1>
                                                <span
                                                    style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                                <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                    We cannot simply send you your old password. A unique link to reset your
                                                    password has been generated for you. To reset your password, click the
                                                    following link and follow the instructions.
                                                </p>
                                                <a
                                                  href="${link}"
                                                  style="background:#2ecd99;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;"
                                                >
                                                  Reset Password
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height:40px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            <tr>
                                <td style="height:20px;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </body>
        </html>`,
    };

    return sendUserEmail(mailOptions);

  } catch(error) {
    return error
  }
}

const sendVerifyEmailLink = async (link, email, firstName, lastName) => {
  try {
    const mailOptions = {
      from: 'TRAVEL AGENCY <travelzagency22@gmail.com>',
      to: email,
      subject: 'Verify your email - Travel agency',
      text: `Here is the verification link ${link}`,
      html: `
        <!doctype html>
        <html lang="en-US">
        <head>
            <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
            <title>Travel agency email verification</title>
            <meta name="description" content="Travel agency email verification">
            <style type="text/css">
                a:hover {text-decoration: underline !important;}
            </style>
        </head>

        <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8; text-align: left" leftmargin="0">
            <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8">
                <tr>
                    <td>
                        <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0"
                            align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <table width="95%" border="0" align="left" cellpadding="0" cellspacing="0"
                                        style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                        <tr>
                                            <td style="height:40px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="padding:0 35px; text-align: left">
                                                <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:'Rubik',sans-serif;">
                                                  Hello ${firstName} ${lastName},
                                                </h1>
                                                
                                                <p style="color:#455056; font-size:15px;line-height:24px; margin: 20px 0 0 0;">
                                                    Please click the button below to verify your email address.
                                                </p>
                                                <a
                                                  href="${link}"
                                                  style="background:#2ecd99;text-decoration:none !important; font-weight:500; margin:20px 0; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;"
                                                >
                                                  Verify Email Address
                                                </a>
                                                <p style="color:#455056; font-size:15px;line-height:24px; margin:0 0 20px 0;">
                                                    If you did not create an account, no further action is required.
                                                </p>
                                                <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                   Regards,
                                                </p>
                                                <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                   Travel agency.
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height:40px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            <tr>
                                <td style="height:20px;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </body>
        </html>`,
    };

    return sendUserEmail(mailOptions);

  } catch(error) {
    return error
  }
}

router.post('/register', async (req, res, next) => {
  const { body, headers } = req;
  const {
    firstName, lastName, email, password,
  } = body
  const { origin } = headers
  const userInfo = await User.findOne({ email })
  if (userInfo) {
    res.status(404).json({
      message: 'Email id is registered with travel agency.'
    })
    return
  }
  const hashedPassword = bcrypt.hashSync(password, 10)
  const user = new User({
    _id: new mongoose.Types.ObjectId(),
    ...body,
    password: hashedPassword,
    isActive: false,
  });
 
  try {
    const userInfo = await user.save()
    const { _id: userId } = userInfo

    const payload = {
      userId,
      email,
    }
  
    const token = jwt.sign(payload, req.app.get('secretKey'))
  
    const link = `${origin}/#/verify-email/${token}`

    await sendVerifyEmailLink(link, email, firstName, lastName)

    CustomerCategory.insertMany([
      { name: 'regular', consumerId: userId },
      { name: 'operator', consumerId: userId }
    ])

    ExpenseCategory.insertMany([
      { name: 'vehicle', consumerId: userId },
      { name: 'staff', consumerId: userId },
      { name: 'other', consumerId: userId }
    ])

    ExpenseType.insertMany([
      { name: 'fuel', consumerId: userId },
      { name: 'salary', consumerId: userId },
      { name: 'maintenance', consumerId: userId },
      { name: 'other', consumerId: userId }
    ])

    PackageCategory.insertMany([
      { name: 'local', consumerId: userId },
      { name: 'out station', consumerId: userId },
      { name: 'fixed', consumerId: userId },
    ])

    PaymentMethod.insertMany([
      { name: 'debit card', consumerId: userId },
      { name: 'credit card', consumerId: userId },
      { name: 'cash', consumerId: userId },
      { name: 'cheque', consumerId: userId },
      { name: 'internet banking', consumerId: userId },
    ])

    RequestType.insertMany([
      { name: 'local', consumerId: userId },
      { name: 'out station', consumerId: userId },
    ])

    StaffCategory.insertMany([
      { name: 'driver', consumerId: userId },
      { name: 'office staff', consumerId: userId },
    ])

    VehicleCategory.insertMany([
      { name: 'own', consumerId: userId },
      { name: 'other', consumerId: userId },
    ])

    VehicleType.insertMany([
      { name: 'car', consumerId: userId },
      { name: 'SUV', consumerId: userId },
      { name: 'MPV', consumerId: userId },
      { name: 'bus', consumerId: userId },
      { name: 'tempo traveller', consumerId: userId },
    ])
  
    res.status(200).json({
      message: 'Email verification link is sent to the email address',
    })
  } catch (error) {
    console.log(error)
    next(error)
  }
});

router.post('/authenticate', async (req, res, next) => {
  const { body, headers } = req;
  const { origin } = headers
  const { email, password } = body

  try {
    const userInfo = await User.findOne({ email })

    if (!userInfo) {
      res.status(401).json({
        message: 'Invalid user'
      })
      return
    }

    const {
      isActive, password: userPassword, _id: userId, firstName, lastName
    } = userInfo

    if (!isActive) {
      const payload = {
        userId,
        email,
      }
    
      const token = jwt.sign(payload, req.app.get('secretKey'))
    
      const link = `${origin}/#/verify-email/${token}`
  
      await sendVerifyEmailLink(link, email, firstName, lastName)

      res.status(200).json({
        code: 'EMAIL_NOT_VERIFIED',
      })
      return
    }

    const isValidPassword = bcrypt.compareSync(password, userPassword)
    if(isValidPassword) {
      const token = jwt.sign({ id: userInfo._id }, req.app.get('secretKey'), { expiresIn: '1h' })
      res.status(200).json({
        user: userInfo,
        token:token,
      })
    } else {
      res.status(401).json({
        message: 'Invalid password',
      })
    }
  } catch (error) {
    next(error)
  }
})

router.post('/forgot-password', async (req, res, next) => {
  const { body, headers } = req
  const { email } = body
  const { origin } = headers
  try {
    const user = await User.findOne({ email })
    if (!user) {
      res.status(404).json({
        message: 'Email does not exist.'
      })
      return
    }
  
    const { isActive, _id: userId } = user

    if (!isActive) {
      const payload = {
        userId,
        email,
      }
    
      const token = jwt.sign(payload, req.app.get('secretKey'))
    
      const link = `${origin}/#/verify-email/${token}`
  
      await sendVerifyEmailLink(link, email, firstName, lastName)

      res.status(200).json({
        code: 'EMAIL_NOT_VERIFIED',
      })
      return
    }
    
    const payload = {
      id: userId,
      email,
    }
  
    const token = jwt.sign(payload, req.app.get('secretKey'), { expiresIn: '1h' })
  
    const link = `${origin}/#/reset-password/${userId}/${token}`

    await sendEmail(link, email)
  
    res.status(200).json({
      message: 'Reset password link is sent to the email address',
    })
  } catch (error) {
    next(error)
  }
})

router.post('/verify-email/:token', async (req, res, next) => {
  const { params } = req;
  const { token } = params
  try {
    jwt.verify(token, req.app.get('secretKey'), async (err, decoded) => {
      if(err) {
        res.status(404).json({
          message: 'Please try again',
        })
      } else {
        const { userId } = decoded
        const requestBody = {
          isActive: true,
        }
        await User.updateOne({ _id: userId }, {
          $set: requestBody
        })
        res.status(200).json({
          message: 'Account activated',
        })
      }
    })

  } catch (error) {
    next(error)
  }
})

router.post('/reset-password/:userId/:token', async (req, res, next) => {
  const { params, body } = req;
  const { userId, token } = params
  const { password } = body
  const hashedPassword = bcrypt.hashSync(password, 10)
  try {
    const user  = await User.findById(userId)
    if (!user || String(userId) !== String(user._id)) {
      res.status(404).json({
        message: 'Invalid user'
      })
      return
    }

    jwt.verify(token, req.app.get('secretKey'), async (err, decoded) => {
      if(err) {
        res.status(404).json({
          message: 'Please try again',
        })
      } else {
        const requestBody = {
          password: hashedPassword,
        }
        const data = await User.updateOne({ _id: decoded.id }, {
          $set: requestBody
        })
        res.status(200).json({
          message: 'Password reset is successful',
          data
        })
      }
    })

  } catch (error) {
    next(error)
  }
})

module.exports = router;
