const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();

const { getYear, getMonth } = require('../utils/date');

const FixedVehiclePayment = require('../models/fixedVehiclePayment');

const Report = require('../models/report');
const VehicleReport = require('../models/vehicleReport');

router.get('/', async (req, res, next) => {
  const { query = {}, body } = req;
  const { userId } = body
  const requestQuery = {
    ...query,
    consumerId: userId,
  }
  const data = await FixedVehiclePayment
  .find(requestQuery)
  .populate('vehicle requestPackage')
  .select('-__v')
  .lean();
  try {
    const response = {
      data
    }
    res.send(response);
  } catch (error) {
    next(error);
  }
});

router.get('/:id', async (req, res, next) => {
  const id = req.params.id;
  try {
    const data = await FixedVehiclePayment.findById(id).select('-__v').lean();
    res.status(200).json(data);
  } catch (error) {
    next(error);
  }
});

router.post('/', async (req, res, next) => {
  const { body, userId } = req
  const fixedVehiclePayment = new FixedVehiclePayment({
    _id: new mongoose.Types.ObjectId(),
    ...body,
    consumerId: userId,
  });

  try {
    const data = await fixedVehiclePayment.save();
    res.status(201).json({
      message: 'Added successfully',
      data,
    });
  } catch (error) {
    next(error);
  }
});

router.patch('/:id', async (req, res, next) => {
  const { params: { id }, body } = req
  const {
    paymentDetails, totalAmount, vehicleDetails = {}, userId,
  } = body
  const { status, paymentDate } = paymentDetails
  const { vehicleCategory, vehicle } = vehicleDetails
  try {
    if (status === 'PAYMENT_RECEIVED') {
      const requestYear = getYear(paymentDate)
      const requestMonth = getMonth(paymentDate)
  
      const filter = {
        year: requestYear,
        month: requestMonth,
        consumerId: userId,
      };
  
      const requestData = {
        $inc: {
          income: totalAmount,
        },
      };

      await Report.findOneAndUpdate(filter, requestData, {
        upsert: true,
      });

      const vehicleReportFilter = {
        year: requestYear,
        month: requestMonth,
        vehicle,
        vehicleCategory,
        consumerId: userId,
      }
  
      await VehicleReport.findOneAndUpdate(vehicleReportFilter, requestData, {
        upsert: true,
      });
  
    }

    const data = await FixedVehiclePayment.updateOne(
      { _id: id },
      {
        $set: {
          paymentDetails
        },
      }
    );

    res.status(200).json({
      message: 'Updated successful',
      data
    })
  } catch (error) {
    next(error);
  }
});

/*
router.delete('/:id', async (req, res, next) => {
  const id = req.params.id;
  try {
    await FixedVehiclePayment.deleteOne({ _id: id });
    res.status(200).json({
      message: 'Deleted successfully',
      id,
    });
  } catch (error) {
    next(error);
  }
});
*/

module.exports = router;
