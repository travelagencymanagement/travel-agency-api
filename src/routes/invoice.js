const express = require('express');
const path = require('path');
const router = express.Router();
const jwt = require('jsonwebtoken');

const RegularRequest = require('../models/regularRequest');
const FixedRequest = require('../models/fixedRequest');

const User = require('../models/user');

const { formatAmount, numberToTime } = require('../utils');
const { formatDate } = require('../utils/date');

router.get('/download', async (req, res, next) => {
  const { query } = req
  const { token, type, id } = query
  jwt.verify(token, req.app.get('secretKey'), async (err, decoded) => {
    const { id: userId } = decoded
    if(err) {
      res.status(404).json({
        message: 'User is not authenticated.',
      })
    } else {
      const user  = await User.findById(userId)
      const isRegular = type === 'regular'
      const data = isRegular ? await RegularRequest
          .findById(id)
          .populate('customer requestType package staff vehicle packageFromProvidedVehicle.package')
          .select('-__v')
          .lean() : await FixedRequest
          .findById(id)
          .populate('customer requestType vehicle staff packageFromProvidedVehicle.package')
          .select('-__v')
          .lean();
      const {
        email: agencyEmail, profile, agencyName, agencyRegistrationNo,
        primaryContact, secondaryContact, websiteLink, address: agencyAddress,
      } = user
      const {
        requestNo, customer, customerDetails, otherCharges = {},
        staff = {}, staffDetails = {}, package = {},
        requestType = {}, pickUpLocation, dropOffLocation,
        pickUpDateTime, dropDateTime, totalHr, openingKm, closingKm, totalKm,
        customerBill,
      } = data
      const { name, contact, email, address = {} } = customer || customerDetails
      const { addressLine1, addressLine2, city, state, pinCode } = address
      const { name: staffName } = staff || staffDetails
      const { toll = {}, parking = {}, nightHalt = {}, driverAllowance = {} } = otherCharges
      const { baseAmount, minimumKm, minimumHr, extraHrPerHrRate, extraKmPerKmRate } = package
      const { name: requestTypeName } = requestType

      const extraKm = (totalKm - minimumKm) < 0 ? 0 : (totalKm - minimumKm)
      const extraHr = (totalHr - minimumHr) < 0 ? 0 : (totalHr - minimumHr)
      const extraKmBilling = extraKm * extraKmPerKmRate
      const extraHrBilling = extraHr * extraHrPerHrRate

      const tollAmount = toll.isChargeableToCustomer ? toll.amount : 0
      const parkingAmount = parking.isChargeableToCustomer ? parking.amount : 0
      const nightHaltAmount = nightHalt.isChargeableToCustomer ? nightHalt.amount : 0
      const driverAllowanceAmount = driverAllowance.isChargeableToCustomer ? driverAllowance.amount : 0
      const totalOtherCharges = tollAmount + parkingAmount + nightHaltAmount + driverAllowanceAmount

      res.pdfFromHTML({
        filename: `invoice-${type}-${requestNo}.pdf`,
        // html: path.resolve(process.cwd(), 'src', 'pdf-template', 'invoice.html'),
        htmlContent: `<html>
        <head>
          <style>
            *{margin:0;padding:0}body{color:#666;font-size:14px}table{border-collapse:collapse;width:100%}.content{padding:10px}.header{border-bottom:1px solid #ccc}.header td{padding-bottom:10px}.header p{color:#666;font-size:12px}.header p.invoice-title{font-size:20px;color:#222;padding-bottom:8px}.logo{height:50px}.contact{font-size:12px}.contact a{color:#222}.section-heading{background:#f4f4f4;font-size:12px;font-weight:700;padding:8px;margin-top:10px;border:1px solid #eee;border-top-left-radius:4px;border-top-right-radius:4px}.section-detail{border:1px solid #eee;border-top:none}.section-detail td{padding:10px}.section-detail div{font-size:12px;color:#888}.section-detail p{font-size:12px;padding-top:4px;color:#000}.order-info{margin-top:20px}.info-box{background-color:#f4f4f4;padding:8px;text-align:center;font-size:12px;border-radius:4px}.info-box p{padding-top:4px;font-weight:700}.footer{text-align:center;padding:10px 0;font-size:10px}.footer p{padding-top:2px;font-size:10px}.note{text-align:center;font-size:10px;padding-top:10px;margin-top:10px;border-top:1px solid #dedede}
          </style>
        </head>
        <body>
          <div class="content">
            <table>
              <tr class="header">
                <td align="left">
                  <table>
                    <tr>
                      <td>
                        <img
                          src=${profile}
                          class="logo"
                        />
                      </td>
                      <td align="left" valign="top">
                        <p class="contact">
                          ${agencyRegistrationNo}
                        </p>
                        <p class="contact">
                          +91 ${primaryContact} / ${secondaryContact}
                        </p>
                        <p class="contact">
                          <a href="mailto:${agencyEmail}">${agencyEmail}</a>
                        </p>
                        <p class="contact">
                          <a href="${websiteLink}">${websiteLink}</a>
                        </p>
                      </td>
                    </tr>
                  </table>
                </td>
                <td align="right" valign="top">
                  <p class="invoice-title">Invoice</p>
                  <p>Order number: ${requestNo}</p>
                  <p>Order date: ${formatDate(pickUpDateTime, 'DD-MMM-YYYY')}</p>
                </td>
              </tr>
            </table>
            <div class="section-heading">
              Customer details
            </div>
            <table class="section-detail">
              <tr>
                <td>
                  <div>
                    Customer name
                  </div>
                  <p>
                    ${name}
                  </p>
                </td>
                <td>
                  <div>
                    Contact
                  </div>
                  <p>
                    ${contact}
                  </p>
                </td>
                <td>
                  <div>
                    E-mail
                  </div>
                  <p>
                    ${email}
                  </p>
                </td>
              </tr>
              <tr>
                <td colspan="3">
                  <div>
                    Address
                  </div>
                  <p>
                  ${addressLine1}, ${addressLine2}, ${city}, ${state}, ${pinCode} 
                  </p>
                </td>
              </tr>
            </table>
            <div class="section-heading">
              Trip details
            </div>
            <table class="section-detail">
              <tr>
                <td>
                  <div>
                    Request type
                  </div>
                  <p>
                    ${requestTypeName}
                  </p>
                </td>
                <td>
                  <div>
                    Pickup location
                  </div>
                  <p>
                    ${pickUpLocation}
                  </p>
                </td>
                <td>
                  <div>
                    Drop location
                  </div>
                  <p>
                    ${dropOffLocation}
                  </p>
                </td>
              </tr>
              <tr>
                <td>
                  <div>
                    Pick up date-time
                  </div>
                  <p>
                    ${formatDate(pickUpDateTime, 'DD-MMM-YYYY, hh:mm A')}
                  </p>
                </td>
                <td>
                  <div>
                    Drop off date-time
                  </div>
                  <p>
                    ${formatDate(dropDateTime, 'DD-MMM-YYYY, hh:mm A')}
                  </p>
                </td>
                <td>
                  <div>
                    Total time
                  </div>
                  <p>
                    ${numberToTime(totalHr)}
                  </p>
                </td>
              </tr>
              <tr>
                <td>
                  <div>
                    Opening KM
                  </div>
                  <p>
                    ${openingKm}
                  </p>
                </td>
                <td>
                  <div>
                    Closing KM
                  </div>
                  <p>
                    ${closingKm}
                  </p>
                </td>
                <td>
                  <div>
                    Total kilometers
                  </div>
                  <p>
                    ${totalKm}
                  </p>
                </td>
              </tr>
            </table>
            ${isRegular ? `<div class="section-heading">Package details</div>` : ''}
            ${isRegular ?
            `<table class="section-detail">
              <tr>
                <td>
                  <div>
                    Base amount
                  </div>
                  <p>
                    ₹${formatAmount(baseAmount)}
                  </p>
                </td>
                <td>
                  <div>
                    Minimum Kilometer
                  </div>
                  <p>
                    ${minimumKm}
                  </p>
                </td>
                <td>
                  <div>
                    Minimum Hour
                  </div>
                  <p>
                    ${minimumHr}
                  </p>
                </td>
              </tr>
              <tr>
                <td>
                  <div>
                    Extra Kilometer rate
                  </div>
                  <p>
                    ₹${formatAmount(extraKmPerKmRate)} / KM
                  </p>
                </td>
                <td>
                  <div>
                    Extra Hour rate
                  </div>
                  <p>
                    ₹${formatAmount(extraHrPerHrRate)} / HR
                  </p>
                </td>
                <td>&nbsp;</td>
              </tr>
            </table>` : ''}
            <div class="section-heading">
              Other details
            </div>
            <table class="section-detail">
              <tr>
                <td>
                  <div>
                    Toll
                  </div>
                  <p>
                  ₹${toll.isChargeableToCustomer ? formatAmount(toll.amount) : formatAmount(0)}
                  </p>
                </td>
                <td>
                  <div>
                    Parking
                  </div>
                  <p>
                  ₹${parking.isChargeableToCustomer ? formatAmount(toll.parking) : formatAmount(0)}
                  </p>
                </td>
                <td>
                  <div>
                    Driver allowance
                  </div>
                  <p>
                  ₹${driverAllowance.isChargeableToCustomer ? formatAmount(driverAllowance.parking) : formatAmount(0)}
                  </p>
                </td>
              </tr>
              <tr>
                <td>
                  <div>
                    Night halt
                  </div>
                  <p>
                  ₹${nightHalt.isChargeableToCustomer ? formatAmount(nightHalt.parking) : formatAmount(0)}
                  </p>
                </td>
                <td>&nbsp;</td>
                <td>
                  <div>
                    Driver name
                  </div>
                  <p>
                    ${staffName}
                  </p>
                </td>
              </tr>
            </table>
            
            <table class="order-info">
              <tr>
                <td class="info-box">
                  Basic amount
                  <p>
                    ₹${formatAmount(baseAmount)}
                  </p>
                </td>
                <td align="center">+</td>
                <td class="info-box">
                  Extra charges
                  <p>
                    ₹${formatAmount(extraKmBilling + extraHrBilling)}
                  </p>
                </td>
                <td align="center">+</td>
                <td class="info-box">
                  Other charges
                  <p>
                    ₹${formatAmount(totalOtherCharges)}
                  </p>
                </td>
                <td align="center">=</td>
                <td class="info-box">
                  Total amount
                  <p>
                    ₹${formatAmount(customerBill)}
                  </p>
                </td>
              </tr>
            </table>
          </div>
          <div class="footer" id="pageFooter">
            <strong>${agencyName}</strong>
            <p>
              ${agencyAddress.addressLine1}, ${agencyAddress.addressLine2},
              ${agencyAddress.city}, ${agencyAddress.state} - ${agencyAddress.pinCode}
            </p>
            <div class="note">
              <strong>NOTE:</strong> This is computer generated invoice and does not require physical signature.
            </div>
          </div>
        </body>
      </html>`,
        options: {
          format: 'A4'
        }
      });
    }
  })
});

router.get('/download/fixed-vehicle-payment', async (req, res, next) => {
  res.pdfFromHTML({
    filename: 'invoice.pdf',
    htmlContent: `<html>
    <body>
      <table style="border:1px solid red">
        <tr>
          <th>name</th>
          <th>city</th>
        </tr>
        <tr>
          <td>Santosh</td>
          <td>Mumbai</td>
        </tr>
      </table>
    </body>
    </html>`,
    options: {
      format: 'A4'
    }
  });
});

module.exports = router;
