const express = require('express');
const path = require('path');
const fs = require('fs');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const formidable = require('formidable');

const { google } = require('googleapis');

const router = express.Router();

// These id's and secrets should come from .env file.
const CLIENT_ID = '93508021990-9bn8nv80hgslhg9iflikfsb4ejpluqba.apps.googleusercontent.com';
const CLIENT_SECRET = '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';
const REDIRECT_URI = 'https://developers.google.com/oauthplayground';
const REFRESH_TOKEN = '1//04ra7mTTfFn7ICgYIARAAGAQSNwF-L9IraEGCOGuOiyQNcaR7CiGOnKB_Tg-Xjadz8zpWIykkExvWKDgmYg5fpbnDAAkdyylXOvI';

const FOLDER_ID = '1ExzdnIli5ssEH3c0VIpsz8G2NTkwucMB'

const oAuth2Client = new google.auth.OAuth2(
  CLIENT_ID,
  CLIENT_SECRET,
  REDIRECT_URI
);

oAuth2Client.setCredentials({ refresh_token: REFRESH_TOKEN });

const drive = google.drive({
  version: 'v3',
  auth: oAuth2Client,
});

const User = require('../models/user');

router.patch('/change-password', async (req, res, next) => {
  const { body } = req;
  const { password, userId } = body
  const hashedPassword = bcrypt.hashSync(password, 10)
  const requestBody = {
    password: hashedPassword,
  }
  const data = await User.updateOne({ _id: userId }, {
    $set: requestBody
  })
  res.status(200).json({
    message: 'Password changed successful',
    data
  })
})

router.post('/change-profile', async (req, res, next) => {
  const { userId } = req.body
  const form = formidable();
  form.parse(req, async (err, _, files) => {
    if (err) {
      res.writeHead(err.httpCode || 400, { 'Content-Type': 'text/plain' });
      res.end(String(err));
      return;
    }
    const { profileImage } = files
    const { filepath, mimetype, newFilename } = profileImage
    try {
      const { data } = await drive.files.create({
        requestBody: {
          name: newFilename,
          mimeType: mimetype,
          parents: [FOLDER_ID],
        },
        media: {
          mimeType: 'image/png',
          body: fs.createReadStream(filepath),
        },
      });
      const profile = `https://drive.google.com/uc?export=view&id=${data.id}`
      const requestBody = {
        profile,
      }
      await User.updateOne({ _id: userId }, {
        $set: requestBody
      })
      res.status(200).json({
        profile,
        message: 'image upload successful'
      })
    } catch (error) {
      next(error)
    }
  });
})

router.patch('/update', async (req, res, next) => {
  const { headers, body } = req;
  try {
    const token = headers['x-access-token']
    jwt.verify(token, req.app.get('secretKey'), async (err, decoded) => {
      const data = await User.updateOne({ _id: decoded.id }, {
        $set: body
      })
      res.status(200).json({
        message: 'Updated successfully',
        data
      })
    })
  } catch (error) {
    next(error)
  }
});

module.exports = router;